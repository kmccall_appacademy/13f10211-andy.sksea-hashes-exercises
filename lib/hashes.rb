# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  res = {}
  str.split(" ").each { |w| res[w] = w.length }
  res
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  max_val = hash.values.max
  hash.key(max_val)
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |k, v| older[k] = v }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  res = {}
  word.chars.uniq.each { |c| res[c] = word.count(c) }
  res
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  h = {}
  arr.each { |w| h[w] = "😗" }
  h.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  h = {}
  h[:even] = numbers.select { |n| n % 2 == 0 }.count
  h[:odd] = numbers.length - h[:even]
  h
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
VOWELS = [*"a".."e"]
def most_common_vowel(string)
  h = {}
  VOWELS.each { |c| h[c] = string.count(c) }
  h.select { |_, v| v == h.values.max }.keys[0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  names = students.select { |_, v| v > 6 }.keys
  res = []
  names[0..-2].each.with_index do |n, i|
    names[i+1..-1].each do |m|
      res << [n, m]
    end
  end
  res
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  h = Hash.new(0)
  specimens.each { |s| h[s] += 1 }
  h.length ** 2 * h.values.min.to_f / h.values.max
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  n_h = Hash.new(0)
  normal_sign.downcase.chars.each { |c| n_h[c] += 1 }
  v_h = Hash.new(0)
  vandalized_sign.downcase.chars.each { |c| v_h[c] += 1 }
  v_h.each { |c, count| return false if n_h[c] < count }
  true
end

def character_count(str)
end
